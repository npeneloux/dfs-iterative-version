
public class Sommet implements Comparable<Sommet>{
	private int numero;
	private int pre = -1;
	private int post = -1;
	private boolean marque = false;
	
	//Class Sommet : prend en attribut un numero, correspondant � son identit�, 2 int pre et post initialis� � -1 (la valeur changera pendant l'algorithme
	//et un boolean marque pour voir si le sommet est d�j� marqu� ou non .
	
	//Constructeur :
	public Sommet(int numero) {
		this.numero = numero;
	}
	
	public Sommet(int numero, int pre, int post) {
		this.numero = numero;
		this.pre = pre;
		this.post = post;
	}
	//Getter et Setter
	public int getNum() {
		return this.numero;
	}
	
	public void setNum(int num) {
		this.numero = num;
	}
	
	public int getPre() {
		return this.pre;
	}
	
	public void setPre(int pre) {
		this.pre = pre;
	}
	
	public int getPost() {
		return this.post;
	}
	
	public void setPost(int post) {
		this.post = post;
	}
	
	public boolean getMarque() {
		return this.marque;
	}
	
	public void resetMarquage() {
		this.marque = false;
	}
	
	public void marquage() {
		this.marque = true;
	}
	
	
	public String toString() {
		return ("Sommet " + this.numero + " (" + this.pre + " ; " + this.post + ")\n");
	}

	@Override //Pour pouvoir trier le tableau de Sommet pr�sent dans la fonction de tri dans l'interface Comparable
	public int compareTo(Sommet o) {
		if (this.numero != o.getNum()) {
			return this.numero - o.getNum();
		}
		return 0;
	}
	

}
