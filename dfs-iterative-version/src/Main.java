
public class Main {
	public static void main(String[] args) {
		int c = 0;/* 
		
		
															##########
															#GRAPHE 1#
															########## 
		Graphe g1 = new Graphe();
		Sommet s1 = new Sommet(1);
		Sommet s2 = new Sommet(2);
		Sommet s3 = new Sommet(3);
		Sommet s4 = new Sommet(4);
		Sommet s5 = new Sommet(5);
		
		g1.addSommet(s1);
		g1.addSommet(s2);
		g1.addSommet(s3);
		g1.addSommet(s4);
		g1.addSommet(s5);
		
		Arete a1 = new Arete(s1,s2);
		Arete a1bis = new Arete(s2,s1);
		Arete a2 = new Arete(s1,s3);
		Arete a2bis = new Arete(s3,s1);
		Arete a3 = new Arete(s2,s3);
		Arete a3bis = new Arete(s3,s2);
		Arete a4 = new Arete(s4,s5);
		Arete a4bis = new Arete(s5,s4);
		
		
		g1.addArete(a1);
		g1.addArete(a2);
		g1.addArete(a3);
		g1.addArete(a3bis);
		g1.addArete(a2bis);
		g1.addArete(a1bis);
		g1.addArete(a4);
		g1.addArete(a4bis);
		
		
		
		c = g1.DFS();
		
		
		
		g1.affiche(c);
															##########
															#GRAPHE 2#
															##########
		*/
		
																
		
		Graphe g2 = new Graphe();
		
		Sommet s0 = new Sommet(0);
		Sommet s1 = new Sommet(1);
		Sommet s2 = new Sommet(2);
		Sommet s3 = new Sommet(3);
		Sommet s4 = new Sommet(4);
		Sommet s5 = new Sommet(5);
		Sommet s6 = new Sommet(6);
		Sommet s7 = new Sommet(7);
		Sommet s8 = new Sommet(8);
		Sommet s9 = new Sommet(9);
		Sommet s10 = new Sommet(10);
		Sommet s11 = new Sommet(11);
		
		Arete a1 = new Arete(s0,s4);
		Arete a2 = new Arete(s4,s8);
		Arete a3 = new Arete(s4,s9);
		Arete a4 = new Arete(s8,s9);
		Arete a5 = new Arete(s9,s10);
		Arete a6 = new Arete(s10,s8);
		Arete a7 = new Arete(s10,s6);
		Arete a8 = new Arete(s1,s1);
		Arete a9 = new Arete(s1,s6);
		Arete a10 = new Arete(s1,s7);
		Arete a11 = new Arete(s7,s3);
		Arete a12 = new Arete(s3,s2);
		Arete a13 = new Arete(s11,s7);
		Arete a14 = new Arete(s11,s10);
		Arete a15 = new Arete(s11,s6);
		Arete a16 = new Arete(s0,s5);
		Arete a17 = new Arete(s5,s10);
		
		g2.addSommet(s0);
		g2.addSommet(s1);
		g2.addSommet(s2);

		g2.addSommet(s3);
		g2.addSommet(s4);

		g2.addSommet(s5);
		g2.addSommet(s6);
		g2.addSommet(s7);
		g2.addSommet(s8);
		g2.addSommet(s9);
		g2.addSommet(s10);
		g2.addSommet(s11);
		
		g2.addArete(a1);
		g2.addArete(a2);
		g2.addArete(a3);
		g2.addArete(a4);
		g2.addArete(a5);
		g2.addArete(a6);
		g2.addArete(a7);
		g2.addArete(a8);
		g2.addArete(a9);
		g2.addArete(a10);
		g2.addArete(a11);
		g2.addArete(a12);
		g2.addArete(a13);
		g2.addArete(a14);
		g2.addArete(a15);
		g2.addArete(a17);
		g2.addArete(a16);
		
		c = g2.DFS();
		
		g2.affiche(c);


		
		
	}
}
