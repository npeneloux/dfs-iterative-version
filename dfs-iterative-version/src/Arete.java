import java.util.Arrays;
import java.util.LinkedList;
import java.util.*;
public class Arete {
	private Sommet u;
	private Sommet v;
	//Une arete prend seulement 2 Sommet en attribut, representant les deux extr�mit�s de l'arete.
	public Arete(Sommet u, Sommet v) {
		this.u = u;
		this.v = v;
	}
	
	public Sommet getSommet1() {
		return this.u;
	}
	
	public Sommet getSommet2() {
		return this.v;
	}
	//Fonction qui renvoie un tableau de sommet, contenant tout les sommets de la liste qui sont lies a u.
	//Par la m�me occasion, on tri le tableau. J'ai utilis� les fonctions de tri d�j� pr�sente dans java, ayant �chou� � faire la mienne moi m�me ...
	// C'est pour cela que j'ai besoin d'implementer Comparable pour les Sommets
	public static Sommet[] listeSommetAlphaNum(Sommet u, LinkedList<Arete> liste){
		LinkedList<Arete> liste_succ = new LinkedList<Arete>();
		for (Arete i : liste) {
			if (i.getSommet1() == u) {
				liste_succ.add(i);
			}
		}
		Sommet[] res = new Sommet[liste_succ.size()];
		for(int i = 0; i < res.length;i++) {
			res[i] = liste_succ.pop().getSommet2();
		}
		Arrays.sort(res, Collections.reverseOrder());
		return res;
	
		
	}

}
