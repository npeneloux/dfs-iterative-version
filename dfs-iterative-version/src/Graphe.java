import java.util.LinkedList;
import java.util.Stack;
public class Graphe {
	private LinkedList<Sommet> V;
	public LinkedList<Arete> E;
	
	public Graphe() {
		this.V = new LinkedList<Sommet>();
		this.E = new LinkedList<Arete>();
	}
	
	
	public int explorer(Sommet u, int time) {
		Stack<Sommet> pile = new Stack<Sommet>(); // initialisation pile
		pile.push(u); //On ajoute le sommet initial a la pile
		while (!pile.empty()) { 
			Sommet s = pile.pop(); // on depile la pile
			if (s.getMarque() == false) { // si le sommet n'est pas marqu� (nous ne somme pas pass� dessus), on marque et on ajoute le Pre time.
				s.marquage();
				time++;
				s.setPre(time);
				pile.push(s);				
				Sommet[] liste = Arete.listeSommetAlphaNum(s, E); //On regarde par la suite tout les voisins direct du sommet
				
				for (int i = 0; i < liste.length;i++) {
					if (liste[i].getMarque() == false) {
						pile.push(liste[i]);
					}
				}

			} else if (s.getMarque() == true && s.getPost() == -1) { //si on est deja pass� par l�, on change le Post time.
				time++;
				s.setPost(time);
			}
		}
		return time; //on retourne le temps, pour qu'on puisse le garder dans le cas o� on rappelle explorer sur un autre sommet (graphe non connexe).
	}
	
	//Fonction principale, qui appelera sur chaque sommet s'il le faut.
	// Attention, le premier sommet parcouru sera le premier sommet ajouter � la liste ! le second sera le deuxi�me ajouter, etc ...
	//La fonction return un time pour que l'on puisse voir le nombre d'appel effectu� a explorer(non obligatoire mais pratique pour denicher les problemes).
	public int DFS() {
		int time = 0;
		int compteur_appel_explorer = 0;
		for(Sommet u : this.V) {
			u.resetMarquage();
		}
		for (Sommet u : this.V) {
			if (u.getMarque() == false) {
				time = this.explorer(u,time);
				compteur_appel_explorer++;
			}
		}
		return compteur_appel_explorer;
	}
	
	public void affiche(int c) {
		for(Sommet u : this.V) {
			System.out.println("Sommet : " + u.getNum() + " (" + u.getMarque() + ") " + "[" + u.getPre() +":" + u.getPost() + "]");
		}
		System.out.println("Nombre d'appel : " + c);
	}
	
	public void addArete(Arete e) {
		this.E.add(e);
	}
	
	//Ajoute une arete dans la liste d'arete, mais pour un graphe non oriente.
	public void addArete(Sommet u, Sommet v) {
		this.E.add(new Arete(u,v));
		this.E.add(new Arete(v,u));
	}
	
	public void addSommet(Sommet v) {
		this.V.add(v);
	}
	
}
