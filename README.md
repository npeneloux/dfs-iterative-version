# DFS-Iterative-version

Algorithme du parcours en profondeur version itératif.

Pour compiler le projet, rendez-vous à la racine du projet, puis rentrez la commande :

                    javac -classpath ./dfs.jar:. dfs-iterative-version/src/Main.java

Rendez vous puis dans le dossier src (dfs-iterative-version/src), puis faites :

                    java -classpath ../../dfs.jar Main

En cas de problème (comme une mauvaise version du jdk ...) vous devriez compiler par vous même, en vous rendant dans le dossier src (dfs-iterative-version/src) :

                    javac Arete.java
                    javac Sommet.java
                    javac Graphe.java
                    javac Main.java
                    jar cvf dfs.jar Arete.class Graphe.class Main.class Sommet.class

Mettez le fichier dfs.jar obtenu à la racine du projet, puis refaites les explications du dessus.


### Information supplementaire 

* Pour créer un graphe, appelez le constructeur Graphe() (ex : Graphe g = new Graphe();)
* Pour créer un sommet : Sommet s = new Sommet(int numero);
* Pour créer une arête : Arete a = new Arete(Sommet u, Sommet v);
* Pour ajouter un sommet a la liste des sommets du graphe (V), faites g.addSommet(Sommet u), pour une arete faire g.addArete(Sommet u, Sommet v) (dans le cas d'un graphe non orienté !!!), sinon g.addArete(Arete a) (graphe orienté);
* Veuilez rajouter les sommets dans le graphe dans l'ordre dans lequel vous souhaitez que l'algorithme commence (le premier sommet doit être celui qu'il faut ajouter en premier)
